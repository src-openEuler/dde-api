# Run tests in check section
# disable for bootstrapping
%bcond_with check
#%%global _unpackaged_files_terminate_build 0
%global with_debug 1

%if 0%{?with_debug}
%global debug_package   %{nil}
%endif

%global sname deepin-api

# out of memory on armv7hl
%ifarch %{arm}
%global _smp_mflags -j1
%endif

%global goipath  github.com/linuxdeepin/dde-api
%global forgeurl https://github.com/linuxdeepin/dde-api
%global tag      %{version}

Name:           dde-api
Version:        5.5.32
Release:        1
Summary:        Go-lang bingding for dde-daemon
License:        GPLv3+
URL:            https://github.com/linuxdeepin/dde-api
Source0:        %{name}-%{version}.tar.gz
Source1:        vendor.tar.gz


#fix sound service bug
Patch0001: 0001-fix-https-gitee.com-src-openeuler-dde-api-issues-I6H.patch
Patch0002: 0002-Remove-all-symbol-table-and-relocation-information-f.patch

BuildRequires:  libcanberra-devel
BuildRequires:  deepin-gettext-tools
BuildRequires:  librsvg2-devel
BuildRequires:  sqlite-devel
BuildRequires:  golang golang-devel
BuildRequires:  gdk-pixbuf-xlib
BuildRequires:  kf5-kwayland-devel
BuildRequires:  poppler-glib
BuildRequires:  poppler-glib-devel
BuildRequires:  alsa-lib-devel
BuildRequires:  alsa-lib
BuildRequires:  pulseaudio-libs-devel
BuildRequires:  libgudev-devel
%{?systemd_requires}
Requires:       deepin-desktop-base
Requires:       rfkill
Requires(pre):  shadow-utils

%description
%{summary}.

%package -n %{name}-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}-devel
%{summary}.

This package contains library source intended for
building other packages which use import path with
%{goipath} prefix.

%prep
%autosetup -p1 -n %{name}-%{version}

sed -i 's|/usr/lib|%{_libexecdir}|' misc/*services/*.service \
    misc/systemd/system/deepin-shutdown-sound.service \
    lunar-calendar/main.go \
    theme_thumb/gtk/gtk.go \
    thumbnails/gtk/gtk.go

sed -i 's|PREFIX}${libdir|LIBDIR|; s|libdir|LIBDIR|' \
    Makefile adjust-grub-theme/main.go
tar -xf %{SOURCE1}

%build
go env -w GO111MODULE=auto
export GOPATH=%{_builddir}/%{name}-%{version}/vendor:$GOPATH
%make_build

%install
for file in $(find . -iname "*.go" -o -iname "*.c" -o -iname "*.h" -o -iname "*.s"); do
    install -d -p %{buildroot}/%{gopath}/src/%{goipath}/$(dirname $file)
    cp -pav $file %{buildroot}/%{gopath}/src/%{goipath}/$file
    echo "%{gopath}/src/%{goipath}/$file" >> devel.file-list
done

%make_install SYSTEMD_SERVICE_DIR="%{_unitdir}" LIBDIR="%{_libexecdir}"
# HOME directory for user deepin-sound-player
mkdir -p %{buildroot}%{_sharedstatedir}/deepin-sound-player

%pre
getent group deepin-sound-player >/dev/null || groupadd -r deepin-sound-player
getent passwd deepin-sound-player >/dev/null || \
    useradd -r -g deepin-sound-player -d %{_sharedstatedir}/deepin-sound-player\
    -s /sbin/nologin \
    -c "User of com.deepin.api.SoundThemePlayer.service" deepin-sound-player
exit 0

%post
%systemd_post deepin-shutdown-sound.service

%preun
%systemd_preun deepin-shutdown-sound.service

%postun
%systemd_postun_with_restart deepin-shutdown-sound.service

%files
%doc README.md
%license LICENSE
%{_bindir}/dde-open
%{_libexecdir}/%{sname}/
%{_unitdir}/*.service
%{_datadir}/dbus-1/services/*.service
%{_datadir}/dbus-1/system-services/*.service
%{_datadir}/dbus-1/system.d/*.conf
%{_datadir}/icons/hicolor/*/actions/*
%{_datadir}/dde-api/data/huangli.db
%{_datadir}/dde-api/data/huangli.version
%{_datadir}/dde-api/data/pkg_depends
%{_datadir}/dde-api/data/grub-themes/
%{_datadir}/polkit-1/actions/com.deepin.api.locale-helper.policy
%{_datadir}/polkit-1/actions/com.deepin.api.device.unblock-bluetooth-devices.policy
%{_var}/lib/polkit-1/localauthority/10-vendor.d/com.deepin.api.device.pkla
%attr(-, deepin-sound-player, deepin-sound-player) %{_sharedstatedir}/deepin-sound-player

%files -n %{name}-devel -f devel.file-list
%{_datadir}/gocode/src/%{goipath}/*/testdata/
%{_datadir}/gocode/src/%{goipath}/themes/scanner/testdata/

%changelog
* Mon Jul 31 2023 leeffo <liweiganga@uniontech.com> - 5.5.32-1
- upgrade to version 5.5.32

* Wed May 31 2023 Wenlong Zhang<zhangwenlong@loongson.cn> - 5.4.30-2
- update sys to 0.4.0 to support loong64

* Tue Apr 04 2023 liweiganga <liweiganga@uniontech.com> - 5.4.30-1
- update: update 5.4.30

* Tue Apr 04 2023 liweiganga <liweiganga@uniontech.com> - 5.4.6-5
- feat: fix pie

* Tue Mar 14 2023 liweiganga <liweiganga@uniontech.com> - 5.4.6-4
- feat: fix strip

* Fri Mar 10 2023 heyitao <heyitao@uniontech.com> - 5.4.6-3
- Fix sound service failed

* Wed Dec 21 2022 liweiganga <liweiganga@uniontech.com> - 5.4.6-2
- enable debuginfo for fix strip

* Mon Jul 18 2022 konglidong <konglidong@uniontech.com> - 5.4.6-1
- Update to 5.4.6

* Sat Jan 29 2022 liweiganga <liweiganga@uniontech.com> - 5.2.0-2
- fix build error.

* Thu Aug 26 2021 weidong <weidong@uniontech.com> - 5.2.0-1
- Update dde-api.

* Thu Mar 4 2021 weidong <weidong@uniontech.com> - 5.1.11.1-8
- Update license.

* Thu Feb 18 2021 panchenbo <panchenbo@uniontech.com> - 5.1.11.1-7
- fix build error
* Thu Sep 3 2020 weidong <weidong@uniontech.com> - 5.1.11.1-6
- fix source url in spec
* Wed Sep 2 2020 chenbo pan <panchenbo@uniontech.com> - 5.1.11.1-5
- remove dde-api-devel
* Wed Sep 2 2020 chenbo pan <panchenbo@uniontech.com> - 5.1.11.1-4
- remove install golang devel
* Tue Aug 18 2020 chenbo pan <panchenbo@uniontech.com> - 5.1.11.1-3
- remove golang devel
* Thu Jul 30 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.1.11.1-2
- fix spec
* Thu Jul 30 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.1.11.1-1
- Package init
